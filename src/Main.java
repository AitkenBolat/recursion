import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void task1(int number,int sum){
        if(number == 0){
            System.out.println(sum);
            return;
        }
        sum += number * number;
        task1(number-1,sum);

    }
    public static void task2(int number, int sum){
        if(number == 0) {
            System.out.println(sum);
            return;
        }
        sum += number;
        task2(number-1, sum);
    }
    public static void task4(int base, int sum,int power){
        if(power < 0){
            System.out.println(sum);
            return;
        }
        sum += Math.pow(base,power);
        task4(base,sum,power-1);
    }
    public static void task5(int number){
        if(number == 0){
            return;
        }
        System.out.print(number + " ");
        task5(number-1);
    }
    public static void task6(int number,String[] characters){
        if(number < 0){
            return;
        }
        System.out.print(characters[number] + " ");
        task6(number-1,characters);

    }
    public static void task7(int number,int[][] array,int interm){
        if(number == interm){
            return;
        }

    }
    public static void task8(int n,int k,int interm,int interm2){
        interm = k;
        if(interm2 == 0){
            return;
        }
        task8k(n,k,interm,interm2);

    }
    public static void task8k(int n,int k,int interm,int interm2){
        if(interm == 0){
            return;
        }
        System.out.println(n + " " +interm2+""+interm);
        task8k(n,k,interm-1,interm2);
        task8(n,k,interm,interm2-1);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int sum = 0;
        task1(number,sum);
        int number2 = scanner.nextInt();
        task2(number2,sum);
        int number3 = scanner.nextInt();
        int power = scanner.nextInt();
        task4(number3,sum,power);
        String[] array = new String[]{"Abc","bcdh","abcdef"};
        int number4 = array.length;
        task6(number4-1,array);
        int number5 = scanner.nextInt();

        int interm = 1;
        int[][] array1 = new int[number5][number5];
        task7(number5,array1,interm);
        int n = 3;
        int k = 3;
        int interm1 = k;
        int interm2 = n;
        task8(n,k,interm1,interm2);
    }
}